package ie.revenue.autotest.modules.core.specs.test;

import ie.revenue.autotest.modules.core.CalHelper;
import ie.revenue.autotest.modules.core.CoreConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = CoreConfig.class)
public class TestSpec extends AbstractTestNGSpringContextTests {

    @Autowired
    private CalHelper calHelper;

    @Test
    public void testSpec(){
        Assert.assertEquals(calHelper.add(3,10), 13);
    }

}
