package ie.revenue.autotest.modules.specs;

import ie.revenue.autotest.modules.core.CoreConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:/core.properties"})
@ComponentScan("ie.revenue.autotest.modules.specs")
@Import(CoreConfig.class)
public class SpecConfig {
}
