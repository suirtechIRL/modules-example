package ie.revenue.autotest.modules.core.test;

import ie.revenue.autotest.modules.core.CalHelper;
import ie.revenue.autotest.modules.core.Car;
import ie.revenue.autotest.modules.core.CoreConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = CoreConfig.class)
public class ModuleTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private CalHelper calHelper;

    @Autowired
    @Qualifier("HondaCivic")
    private Car hondaCivic;

    @Autowired
    @Qualifier("audiA5")
    private Car audiA5;

    @Test
    public void testAdd(){
        Assert.assertEquals(calHelper.add(3,5), 8);
    }

    @Test
    public void testMultiply(){
        Assert.assertEquals(calHelper.multiply(5), 15);
    }

    @Test
    public void testCarHondaCivic(){
        Assert.assertEquals(hondaCivic.getDoors(), 3);
    }

    @Test
    public void testCarAudiA5(){
        Assert.assertEquals(audiA5.getDoors(), 5);
    }

}
