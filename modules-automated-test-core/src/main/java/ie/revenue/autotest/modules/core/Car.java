package ie.revenue.autotest.modules.core;

public class Car {

    private String make;
    private String model;
    private String colour;
    private int doors;

    public Car(String make, String model, String colour, int doors){
        this.make = make;
        this.model = model;
        this.colour = colour;
        this.doors = doors;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }
}
