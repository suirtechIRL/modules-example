package ie.revenue.autotest.modules.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CalHelper {

    @Value("${multiply}")
    private int multiplier;

    public int add(int a, int b){
        return a+b;
    }

    public int multiply(int a){
        return multiplier * a;
    }
}
