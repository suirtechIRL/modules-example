package ie.revenue.autotest.modules.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:/core.properties"})
@ComponentScan("ie.revenue.autotest.modules.core")
public class CoreConfig {

    @Bean(name="HondaCivic")
    public Car makeHondaCivic(){
        return new Car("Honda", "Civic", "Black", 3);
    }

    @Bean(name="audiA5")
    public Car makeAudiA5(){
        return new Car("Audi", "A5", "Black", 5);
    }
}
